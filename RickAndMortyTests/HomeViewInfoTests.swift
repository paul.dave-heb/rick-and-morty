//
//  RickAndMortyTests.swift
//  RickAndMortyTests
//
//  Created by Paul,Dave on 5/13/22.
//

import Services
@testable import RickAndMorty
import XCTest

class RickAndMortyTests: XCTestCase {


    func testExample() throws {
        let person = Person(name: "rick", image: nil)
        let homeInfo = HomeViewInfo(profileName: "dave", person: person)
        XCTAssertEqual(homeInfo.greeting, "Hey, dave!")

    }

}
