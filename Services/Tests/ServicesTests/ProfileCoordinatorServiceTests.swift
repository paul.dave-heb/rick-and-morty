import XCTest
@testable import Services
import Combine

class TestProfileNetworkService: ProfileNetworkService {
    var networkHits = 0
    var name: String = "Bob"

    func getName() async throws -> String {
        networkHits += 1
        return name
    }

    func setName(_ name: String) async throws {
        networkHits += 1
        self.name = name
    }
}



final class ProfileCoordinatorServicesTests: XCTestCase {
    var latestName: String?
    var cancellables = Set<AnyCancellable>()

    func testExample() async throws {
        let networkService = TestProfileNetworkService()
        let service = ProfileCoordinatorService(networkService: networkService)

        service.profilePublisher.sink(receiveValue: { [weak self] in self?.latestName = $0 }).store(in: &cancellables)

        try await Task.sleep(nanoseconds: 100_000_000)

        try await service.setName("joe")

        XCTAssertEqual(latestName, "joe")
        XCTAssertEqual(networkService.networkHits, 2)


        networkService.name = "Betty"

        _ = try await service.getName()
        XCTAssertEqual(latestName, "Betty")
    }
}
