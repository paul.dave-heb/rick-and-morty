//
//  CharacterService.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/3/22.
//

import Combine
import UIKit

/// CharacterService is the public interface to our service layer!  The app layer can make requests with `loadRandomCharacter()`, or observe
/// service state with the `currentCharacterPublisher`.
public protocol CharacterService {
    func loadRandomCharacter() async throws -> Person

    var currentCharacterPublisher: AnyPublisher<Person, Never> { get }
}

public enum Services {
    public static func characterService() -> CharacterService {
        MyCharacterService(networkService: RestCharacterNetworkService())
    }

    public static func profileService() -> ProfileService {
        ProfileCoordinatorService(networkService: DummyProfileNetworkService())
    }
}

/// MyCharacterService is the "private" implementation of CharacterService.  App layer tests could swap out any mock service that conforms to CharacterService.
internal class MyCharacterService: CharacterService {
    private let networkService: CharacterNetworkService
    private let subject = CurrentValueSubject<Person?, Never>(nil)

    init(networkService: CharacterNetworkService) {
        self.networkService = networkService
    }

    /// Tells the service to load a random character and assign it as the new "current" character.
    func loadRandomCharacter() async throws -> Person {
        let character = try await networkService.getRandomCharacter()
        subject.send(character)
        return character
    }

    /// Subscribe to the currentCharacterPublisher to get updates on the app's "current" character.
    var currentCharacterPublisher: AnyPublisher<Person, Never> {
        // Why don't we just expose our `CurrentValueSubject` directly as our public interface?
        // If we did this, then consumers of this service would be able to *send* data to our publisher.
        // By erasing the subject type, we ensure that our public interface is read-only.
        subject.compactMap { $0 }.eraseToAnyPublisher()
    }
}
