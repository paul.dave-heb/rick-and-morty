//
//  Model.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/4/22.
//

import UIKit

/// Character is our primary data type for the app, used throughout the app and service layers.  I often refer to these as "common currency" types.
public struct Person {
    public init(name: String, image: UIImage?) {
        self.name = name
        self.image = image
    }

    public let name: String
    public let image: UIImage?
}
