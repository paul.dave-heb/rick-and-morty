//
//  File.swift
//  
//
//  Created by Paul,Dave on 5/9/22.
//

import Foundation

protocol ProfileNetworkService {
    func getName() async throws -> String
    func setName(_ name: String) async throws
}

class DummyProfileNetworkService: ProfileNetworkService {
    var name: String = "Bob"

    func getName() async throws -> String {
        name
    }

    func setName(_ name: String) async throws {
        self.name = name
    }
}
