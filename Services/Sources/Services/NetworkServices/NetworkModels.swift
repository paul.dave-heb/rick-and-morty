//
//  NetworkModels.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/9/22.
//

import Foundation

/// CharacterResponse represents JSON downloaded from the back end.  Since it is network specific, it is "private" to the network services layer.
struct CharacterResponse: Codable {
    let name: String
    let image: String
}
