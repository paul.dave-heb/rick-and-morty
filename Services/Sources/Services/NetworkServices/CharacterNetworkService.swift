//
//  CharacterNetworkService.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/4/22.
//

import Foundation
import UIKit

protocol CharacterNetworkService {
    func getRandomCharacter() async throws -> Person
}

class RestCharacterNetworkService: CharacterNetworkService {
    func getRandomCharacter() async throws -> Person {
        let randomId = Int.random(in: 0..<800)

        // 1. get character response
        let (data, _) = try await URLSession.shared.data(for: URLRequest(url: URL(string: "https://rickandmortyapi.com/api/character/\(randomId)")!))
        let characterResponse = try JSONDecoder().decode(CharacterResponse.self, from: data)

        // 2. get image:
        let image: UIImage? = await {
            guard let url = URL(string: characterResponse.image),
                  let (data, _) = try? await URLSession.shared.data(for: URLRequest(url: url)) else { return nil }
            return UIImage(data: data)
        }()

        return Person(name: characterResponse.name, image: image)
    }
}
