//
//  File.swift
//  
//
//  Created by Paul,Dave on 5/9/22.
//

import Combine
import Foundation

public protocol ProfileService {
    func getName() async throws -> String
    func setName(_ name: String) async throws
    var profilePublisher: AnyPublisher<String, Never> { get }
}

class ProfileCoordinatorService: ProfileService {
    let networkService: ProfileNetworkService
    let subject = CurrentValueSubject<String?, Never>(nil)

    init(networkService: ProfileNetworkService) {
        self.networkService = networkService

        // Since we're not caching the profile YET, let's get the current name whenever we instantiate this service:
        Task {
            try? await getName()
        }
    }

    func getName() async throws -> String {
        let name = try await networkService.getName()
        subject.send(name)
        return name
    }

    func setName(_ name: String) async throws {
        try await networkService.setName(name)
        subject.send(name)
    }

    var profilePublisher: AnyPublisher<String, Never> {
        subject.compactMap { $0 }.eraseToAnyPublisher()
    }
}
