//
//  ProfileView.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/10/22.
//

import Combine
import SwiftUI

struct ProfileView: View {
    let publisher: AnyPublisher<String, Never>
    let saveHandler: (String) async throws -> Void
    @State var isEditing: Bool = false
    @State var name: String = "Ruth"
    @State var modifiedName: String = ""

    var body: some View {
        Form {
            Section {
                if isEditing {
                    TextField("name", text: $modifiedName)
                } else {
                    Text("Name: \(name)")
                }

            }
            Section {
                if isEditing {
                    Button("Save") {
                        Task {
                            try? await saveHandler(modifiedName)
                            isEditing = false
                        }
                    }
                    Button("Cancel") {
                        isEditing = false
                    }

                } else {
                    Button("Edit") {
                        modifiedName = name
                        isEditing = true
                    }
                }
            }
        }.onReceive(publisher) { name = $0 }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        let subject = CurrentValueSubject<String, Never>("Ruth")
        ProfileView(publisher: subject.eraseToAnyPublisher()) { subject.send($0) }
    }
}
