This project is meant to demonstrate the MyHEB team's approach to a testable and observable application "service" layer.

# Unidirectional Data Flow

Data flows into views through a publisher.  The views simply update according to the data received.

This example uses UIKit, but in SwiftUI we would use the `onReceive` modifier to update state, or subscribe to a publisher inside of an Observable/State Object.

Notice how the view controller can request an action that the service will carry out, but the view controller does not receive new data directly from that interaction.  Instead, the action will trigger new data on the view controller's publisher.  However, the view _is_ responsible for handling errors that may arise from the action request.

# Service Interfaces

## "Public" Interface - CharacterService

CharacterService is the main interface that the app layer uses to access the service layer.  By using a protocol, we can swap our normal concrete implementation with an alternative instance for testing or debugging.

## "Network" Interface - CharacterNetworkService

Where CharacterService provides the general public interface to our service, CharacterNetworkService provides a more targeted API specifically for the network slice of the service layer.  Again, by using a protocol we can test more easily.  

# Concurrency Mechanisms

For this demo, I've chosen to use Apple's Combine framework to facilitate model observation, and Swift async/await to perform asynchronous "transactional" requests.  However, these are certainly not the only choices!  Other options might be:
* "All in" on Combine - use Combine Publishers for continued observation and transactional requests
* "All in" on Swift Concurrency - use Swift AsyncSequence for continued observation
* Closures for one or both of the two mentioned use cases
* NSNotificationCenter for observations!
IMHO the current choices are generally well suited, but we should be mindful of the advantages of tradeoffs of each alternative.

 
  


  

