//
//  HomeViewModel.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/9/22.
//

import UIKit
import Services

enum HomeViewInfo {
    struct Content {
        let greeting: String
        let characterName: String
        let image: UIImage?
    }

    case loading
    case loaded(Content)

    init(profileName: String, person: Person?) {
        self = .loaded(Content(greeting: "Hello \(profileName)", characterName: person?.name ?? "", image: person?.image))
    }
}


