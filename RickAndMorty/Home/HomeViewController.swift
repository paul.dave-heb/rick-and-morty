//
//  ViewController.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/3/22.
//

import Combine
import Services
import UIKit

protocol ViewControllerDelegate: AnyObject {
    func viewControllerRequestsLoadCharacter(_ viewController: HomeViewController) async throws
}

class HomeViewController: UIViewController {
    let publisher: AnyPublisher<HomeViewInfo, Never>
    var cancellables = Set<AnyCancellable>()

    weak var delegate: ViewControllerDelegate?

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.backgroundColor = .white
        stackView.alignment = .center
        return stackView
    }()

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    lazy var label = UILabel()
    lazy var button: UIButton = {
        let button = UIButton()
        button.setTitle("Load a character", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.setTitleColor(.black, for: .normal)
        return button
    }()

    lazy var greetingLabel = UILabel()

    init(publisher: AnyPublisher<HomeViewInfo, Never>) {
        self.publisher = publisher
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        view.addSubview(stackView)

        [greetingLabel, label, imageView, button].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            stackView.addArrangedSubview($0)
        }

        publisher
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [greetingLabel, label, imageView] in
                switch $0 {
                case .loading: label.text = "loading"
                case .loaded(let content):
                    label.text = content.characterName
                    imageView.image = content.image
                    greetingLabel.text = content.greeting
                }

            }).store(in: &cancellables)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        stackView.frame = view.frame.inset(by: view.layoutMargins)
    }

    @objc func buttonTapped() {
        Task {
            do {
                _ = try await delegate?.viewControllerRequestsLoadCharacter(self)
            } catch {
                print("Error: \(error)")
            }
        }
    }
}

