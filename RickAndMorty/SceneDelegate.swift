//
//  SceneDelegate.swift
//  RickAndMorty
//
//  Created by Paul,Dave on 5/3/22.
//

import Combine
import Services
import UIKit
import SwiftUI

extension Publisher where Output == Person, Failure == Never {
    func homeViewModelPublisher(with profilePublisher: AnyPublisher<String, Never>) -> AnyPublisher<HomeViewInfo, Never> {
        self.map { $0 as Person? }  // convert our character stream to allow optional types
            .prepend(nil)  // start with a nil character
            .combineLatest(profilePublisher)
            .map { char, profile in
                    .init(profileName: profile, person: char)
            }
            .prepend(.loading)
            .eraseToAnyPublisher()
    }
}


class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    lazy var characterService = Services.characterService()
    lazy var profileService = Services.profileService()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = scene as? UIWindowScene else { return }
        let window = UIWindow(windowScene: windowScene)

        // Home
        let homePublisher = characterService.currentCharacterPublisher.homeViewModelPublisher(with: profileService.profilePublisher)
        let homeViewController = HomeViewController(publisher: homePublisher)
        homeViewController.delegate = self
        homeViewController.tabBarItem = .init(title: "Home", image: UIImage(systemName: "house"), tag: 0)

        // Profile
        let profileHostingController = UIHostingController(rootView: ProfileView(publisher: profileService.profilePublisher,
                                                                                 saveHandler: { [profileService] name in
            try await profileService.setName(name)
        }))
        profileHostingController.tabBarItem = .init(title: "Profile", image: UIImage(systemName: "person"), tag: 1)

        // Tab Bar Controller
        let tabbarControlller = UITabBarController()
        tabbarControlller.viewControllers = [homeViewController, profileHostingController]

        window.rootViewController = tabbarControlller
        self.window = window
        window.makeKeyAndVisible()
    }
}

extension SceneDelegate: ViewControllerDelegate {
    func viewControllerRequestsLoadCharacter(_ viewController: HomeViewController) async throws {
        _ = try await characterService.loadRandomCharacter()
    }
}
